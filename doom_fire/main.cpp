// Arkadia
#include "ark_core/ark_core.hpp"
// Doom Fire Demo
#include "Demo.hpp"

//
// Helper Functions
//
//------------------------------------------------------------------------------
void
SetBufferSize(ark::Gfx::Window *window, dfd::Demo *demo)
{
    u32 const desired_w = dfd::Demo::DesiredWidth;
    u32 const desired_h = dfd::Demo::DesiredHeight;
    u32 const window_w  = window->GetWidth ();
    u32 const window_h  = window->GetHeight();

    u32 const max_buffer_area  = ark::Area(desired_w, desired_h);
    u32 const curr_buffer_area = ark::Area(window_w,  window_h);

    ark::Vec2_i32 buffer_size(window_w, window_h);
    if(curr_buffer_area > max_buffer_area) {
        buffer_size.x *= f32(desired_w) / f32(window_w);
        buffer_size.y *= f32(desired_h) / f32(window_h);
    }

    window->CreateScreenBuffer(buffer_size.x, buffer_size.y, 4);
    demo  ->SetSize           (buffer_size.x, buffer_size.y);
}


//
// Entry Point
//
//------------------------------------------------------------------------------
int
ark_main(ark::String const &command_line)
{
    ARK_UNUSED(command_line);

    ark::Gfx::Window::CreateOptions create_options = {};
    create_options.caption = dfd::Demo::GetWindowTitle();

    #if ARK_CURRENT_OS != ARK_OS_DOS
        ark::Vec2_i32 const desktop_size(800, 600); //ark::Gfx::GetDesktopSize();
        ark::Vec2_i32 const final_size = desktop_size;
        create_options.width   = final_size.x;
        create_options.height  = final_size.y;
    #elif ARK_CURRENT_OS == ARK_OS_DOS
        create_options.width   = 320;
        create_options.height  = 200;
    #endif

    create_options.buffer_width  = dfd::Demo::DesiredWidth;
    create_options.buffer_height = dfd::Demo::DesiredHeight;
    create_options.buffer_bpp    = 4;

    dfd::Demo             demo;
    ark::Gfx::WindowEvent event;
    ark::Clock            clock;

    ark::Gfx::Window *window = ark::Gfx::Window::Create(create_options);
    window->Show();

    SetBufferSize(window, &demo);
    while(!window->WantsToClose()) {
        while(window->HandleEvents(&event)) {
            // Empty...
        }
        demo.Update(clock.Elapsed());
        demo.Render(window->GetCurrentScreenBuffer());

        window->FlushBuffers();

        clock.Update();
    }

    return 0;
}
