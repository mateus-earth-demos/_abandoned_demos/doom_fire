﻿// Header
#include "Demo.hpp"
// Arkadia
#include "ark_core/IO.hpp"
// Demo
#include "ColorPal.hpp"
#include "Version.hpp"
// Usings
using namespace dfd;


//
// Public Methods
//
//------------------------------------------------------------------------------
ark::String
Demo::GetWindowTitle()
{
    return ark::String::Format(
        "{} - {} - {} - https://stdmatt.com",
        BuildInfo_doom_fire.ProgramName,
        BuildInfo_doom_fire.BuildSimpleVersionString  (),
        BuildInfo_doom_fire.BuildSimpleCopyrightString()
    );
}

//
// Public Methods
//
//------------------------------------------------------------------------------
void Demo::Update(f32 const dt)
{
    _fps_time -= dt;
    ARK_ONLY_IN_DEBUG(
        if(_fps_time <= 0) {
            _fps_time += 1;
            ark::PrintLn("Update: {} - Render: {}", _update_count, _render_count);
            _update_count = 0;
            _render_count = 0;
        }
    );
    _update_time += dt;
    if(_update_time < _target_update_time) {
        return;
    }
    _update_time = 0; // _target_update_time;

    //
    u32 const fire_start_size = 10;
    for (u32 y = (_fire_h - 1) - fire_start_size; y < _fire_h ; y++) {
        for(u32 x = 0; x < _fire_w; x++) {
            u32 const src = y * _fire_w + x;
            _fire_buffer[src] = 36;
        }
    }

    for (u32 y = 1; y < _fire_h ; y++) {
        for(u32 x = 0; x < _fire_w; x++) {
            u32 const rnd = ark::Random::Next(0, 3);
            u32 const src = (y  ) * _fire_w + x;
            u32 const dst = (y-1) * _fire_w + x + rnd + 1;

            if(_fire_buffer[src] == 0) {
                _fire_buffer[dst] = _fire_buffer[src];
            } else {
                _fire_buffer[dst] = _fire_buffer[src] -(rnd & 1);
            }
        }
    }

    ++_update_count;
}

//------------------------------------------------------------------------------
void Demo::Render(ark::Gfx::OffscreenBuffer *offscreen_buffer)
{
    u32 const width  = offscreen_buffer->width;
    u32 const height = offscreen_buffer->height;

    u32 buffer_index = 0;
    for(u32 y = 0; y < height; ++y) {
        for(u32 x = 0; x < width; ++x) {
            u32        const color_index = _fire_buffer[buffer_index];
            ark::Color const &color      = DemoColors[color_index];

            // @todo(stdmatt): DOS has a severe limitation regarding the colors
            // and the type of the Offscreen Buffer.
            //
            // The other platforms uses a u32 buffer size and the color
            // holds a u32 value with RGB(A) but DOS only have 256 colors
            // and use a palette to draw colors.
            //
            // We need to find a way to make this kinda transparent to
            // the applications itself... Jan 30, 21
            #if (ARK_OS_IS_DOS)
                ark::Gfx::PutPixel8(offscreen_buffer, x, y, color.value);
            #else
                ark::Gfx::PutPixel(offscreen_buffer, x, y, color.value);
            #endif

            ++buffer_index;
        }
    }

    ++_render_count;
}
